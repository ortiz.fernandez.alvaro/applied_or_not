"""
This module contains all the neccesary functions for preprocessing
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas_profiling import ProfileReport
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import pandas as pd

def load_data(file):
    """Load dataset from data/raw"""
    path = 'data/raw/' + file
    return(pd.read_csv(path))

def impute_mean(df):
    """Impute missing values using mean value of each numerical column"""
    def fill_mean(col):
        if col.dtype in ['int', 'float']:
            col = col.fillna(col.mean())
        return col
    return(df.apply(fill_mean, axis=0))

