"""
Test script for applied_or_not project.
Let's try some ML models.
"""

#imports
import numpy as np
import pandas as pd
import pickle
from src.preprocess import load_data
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
from sklearn.metrics import classification_report, roc_auc_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.model_selection import cross_val_score, StratifiedKFold
from hyperopt import fmin, hp, tpe, Trials, STATUS_OK, space_eval
from hyperopt.pyll import scope
from functools import partial

#functions

def prepare_data():
    # Loading data
    df_job_desc = load_data("job_desc.csv")
    df_user = load_data("user.csv")

    df = df_user.merge(df_job_desc, how='outer', on='user_id')
    df.head()

    #v25 and v30 have many missing values
    #df = df.drop(columns=['v25','v30'])

    #instead of encoding job title, let's extract some value from it
    def extract_terms(job_title):
        return name in job_title.lower()

    for term in ['senior', 'junior', 'manager', 'lead','consultant', 'remote', 'm/f/d', 'engineer', 'analyst']:
        name = term
        df[name] = df['job_title_full'].apply(lambda col: extract_terms(col))

    # create new feature telling if salary was nan or not
    df['salary_reported'] = ~df['salary'].isnull()

    # Generate onehot encodings
    enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
    #enc.fit(df[['job_title_full', 'company']])
    enc.fit(df[['company']])

    #encoding = enc.transform(df[['job_title_full', 'company']])
    encoding = enc.transform(df[['company']])

    encoding_names = enc.get_feature_names()
    df_encoded = pd.DataFrame(encoding, columns=encoding_names)
    new_df = pd.concat([df, df_encoded], sort=False, axis=1).drop(columns=['job_title_full', 'company'])

    # Separate into features, and target variable
    # new_df = df
    y = new_df.has_applied.astype('category')
    X = new_df[new_df.columns[~new_df.columns.isin(['user_id', 'has_applied'])]]
    return X,y

def transform_data(X_train, X_test):
    # Preprocess data, based on train so that there is no leakage
    train_idx = X_train.index
    test_idx = X_test.index
    imp_mean = SimpleImputer(missing_values=np.nan, strategy='mean')
    imp_mean.fit(X_train.select_dtypes(exclude='object'))
    X_train = pd.DataFrame(imp_mean.transform(X_train))
    X_test = pd.DataFrame(imp_mean.transform(X_test))
    X_train.columns = X.columns
    X_train.index = train_idx
    X_test.columns = X.columns
    X_test.index = test_idx

    scaler = MinMaxScaler()
    scaler.fit(np.array(X_train['salary']).reshape(-1, 1))
    X_train['salary'] = scaler.transform(np.array(X_train['salary']).reshape(-1, 1))
    X_test['salary'] = scaler.transform(np.array(X_test['salary']).reshape(-1, 1))

    return X_train, X_test

#####################################################################################################################

# Split into train and test before anything else
# PIPELINES WIHT GRIDSEARCHCV

# this is a great way of optimizing not only the model, but also all the previous
# processing steps at once.
# an important methos on pipeline is. Which allows us to print the list
# of parameters we can set for the whole list of estimators (transformers and classifiers)
# in our pipeline

X, y = prepare_data()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, shuffle=True, stratify=y)
X_train, X_test = transform_data(X_train, X_test)

## DEFINE SEARCH SPACE
space_std = {
    'rf':{'max_depth': hp.choice('max_depth', range(1,100)),
    'max_features': hp.uniform('max_features', 0, 1),
    'n_estimators': hp.choice('n_estimators', range(50,1000)),
    'criterion': hp.choice('criterion', ["gini", "entropy"]),
    'min_samples_split': scope.int(hp.uniform('min_samples_split', 2, 20)),
    'min_samples_leaf': scope.int(hp.uniform('min_samples_leaf', 1, 20))
    }
}

## DEFINE MODEL

def define_model(space_std):

    pipeline = Pipeline([
        ('rf', RandomForestClassifier(**space_std['rf'], random_state=42))
    ])

    return pipeline

## DEFINE TARGET FUNCTION

def fobj(space_std, X, y, ncv=5):

    model = define_model(space_std)
    cv_res = cross_val_score(model, X, y, cv=StratifiedKFold(ncv, random_state=42, shuffle=True), scoring='roc_auc', n_jobs=-1)

    return -cv_res.mean()

## RUN OPTIMIZATION

trials = Trials()
best_params = fmin(partial(fobj, X=X_train, y=y_train),
                 space_std, algo=tpe.suggest, max_evals=150,
                 trials=trials)

# Building and fitting classifier with best parameters
best_clf = define_model(space_eval(space_std, best_params)).fit(X_train, y_train)

# Calculating performance on validation set
y_test_pred = best_clf.predict(X_test)
clf_val_score = roc_auc_score(y_test, best_clf.predict_proba(X_test)[:, 1])
print('Cross-val score: {0:.5f}; validation score: {1:.5f}'.\
      format(-trials.best_trial['result']['loss'], clf_val_score))
print('Best parameters:')
print(space_eval(space_std, best_params))

# Export best parameters

best_params = {'criterion': 'entropy', 'max_depth': 28, 'max_features': 0.3722770714504038, 'min_samples_leaf': 11, 'min_samples_split': 12, 'n_estimators': 331}

with open('data/models/best_params.pkl', 'wb') as file:
    pickle.dump(best_params, file, protocol=pickle.HIGHEST_PROTOCOL)

# LET US KNOW WHEN THE OPTIMIZATION IS COMPLETE BY MAKING BEEPING SOUND
# import winsound
# frequency = 1000  # Set Frequency To 2500 Hertz
# duration = 1000  # Set Duration To 1000 ms == 1 second
# winsound.Beep(frequency, duration)