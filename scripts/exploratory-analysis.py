"""
Test script for applied_or_not project.
Let's first do an exploratory data analysis.
"""

#imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from src.preprocess import load_data, impute_mean
from src.exploration import generate_figures, generate_report, plot_pca

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# Loading data
df_job_desc = load_data("job_desc.csv")
df_user = load_data("user.csv")

df = df_user.merge(df_job_desc, how='outer', on='user_id')
df.head()

# Quick checks

### job_desc
df_job_desc.shape
df_job_desc.columns
df_job_desc.describe()
df_job_desc.isnull().sum()/df_job_desc.shape[0]
df_job_desc['salary'].hist()
df_job_desc['job_title_full'].nunique()
df_job_desc['company'].hist()
df_job_desc['company'].nunique()
df_job_desc['company'].value_counts(dropna=False)
df_job_desc.groupby('job_title_full').count().sort_values(ascending=False)

### user_desc
df_user.shape
df_user.columns
cols_to_plot = df_user.columns[~df_user.columns.isin(['user_id', 'has_applied'])]
df_user[cols_to_plot].isnull().sum().sort_values(ascending=False)
df_user.groupby('user_id').size().sort_values(ascending=False)
df_user.groupby('has_applied').size().sort_values(ascending=False)/df_user.shape[0]

# plots figures for all numerical features in user.csv
generate_figures(df)

# let's impute the missing values using means
df = impute_mean(df)
print('There are now {} missing values in the dataset'.format(df.isnull().sum().sum()))

# calculate correlation for numerical features in user.csv
df.corr(method='pearson').shape
df.corr(method='pearson')['has_applied'].sort_values(ascending=False)
sns.heatmap(df.corr(method='pearson'))
plt.savefig('data/reports/figures/corr_heatmap.png')

# Principal component analysis
# first we standardized out features (although v's features range from 0 to 1, salary does not)
# Standardizing the features

plot_pca(df)
X = df.select_dtypes(exclude='object')
X = StandardScaler().fit_transform(X)
pca = PCA(n_components=None, svd_solver='auto', random_state=42)
pca.fit(X)
plt.bar(x=range(0,pca.n_components_),height=pca.explained_variance_ratio_)
plt.ylabel('Amount of variance explained (%)')
plt.xlabel('Number of components')

# Generating pandas profile report
generate_report(df_job_desc, "job_desc_report.html")
generate_report(df_user, "user_report.html", minimal=True) #the full report is over 100MB

