"""
Modelling functions
"""

#imports
import numpy as np
import pandas as pd
import pickle
from src.preprocess import load_data
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
from sklearn.metrics import classification_report, roc_auc_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.model_selection import cross_val_score, StratifiedKFold
from hyperopt import fmin, hp, tpe, Trials, STATUS_OK, space_eval
from hyperopt.pyll import scope
from functools import partial

#functions

def prepare_data():
    # Loading data
    df_job_desc = load_data("job_desc.csv")
    df_user = load_data("user.csv")

    df = df_user.merge(df_job_desc, how='outer', on='user_id')
    df.head()

    #v25 and v30 have many missing values
    #df = df.drop(columns=['v25','v30'])

    #instead of encoding job title, let's extract some value from it
    def extract_terms(job_title):
        return name in job_title.lower()

    for term in ['senior', 'junior', 'manager', 'lead','consultant', 'remote', 'm/f/d', 'engineer', 'analyst']:
        name = term
        df[name] = df['job_title_full'].apply(lambda col: extract_terms(col))

    # create new feature telling if salary was nan or not
    df['salary_reported'] = ~df['salary'].isnull()

    # Generate onehot encodings
    enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
    #enc.fit(df[['job_title_full', 'company']])
    enc.fit(df[['company']])

    #encoding = enc.transform(df[['job_title_full', 'company']])
    encoding = enc.transform(df[['company']])

    encoding_names = enc.get_feature_names()
    df_encoded = pd.DataFrame(encoding, columns=encoding_names)
    new_df = pd.concat([df, df_encoded], sort=False, axis=1).drop(columns=['job_title_full', 'company'])

    # Separate into features, and target variable
    # new_df = df
    y = new_df.has_applied.astype('category')
    X = new_df[new_df.columns[~new_df.columns.isin(['user_id', 'has_applied'])]]
    return X,y

def transform_data(X_train, X_test, X):
    # Preprocess data, based on train so that there is no leakage
    train_idx = X_train.index
    test_idx = X_test.index
    imp_mean = SimpleImputer(missing_values=np.nan, strategy='mean')
    imp_mean.fit(X_train.select_dtypes(exclude='object'))
    X_train = pd.DataFrame(imp_mean.transform(X_train))
    X_test = pd.DataFrame(imp_mean.transform(X_test))
    X_train.columns = X.columns
    X_train.index = train_idx
    X_test.columns = X.columns
    X_test.index = test_idx

    scaler = MinMaxScaler()
    scaler.fit(np.array(X_train['salary']).reshape(-1, 1))
    X_train['salary'] = scaler.transform(np.array(X_train['salary']).reshape(-1, 1))
    X_test['salary'] = scaler.transform(np.array(X_test['salary']).reshape(-1, 1))

    return X_train, X_test

#####################################################################################################################

# Split into train and test before anything else
# PIPELINES WIHT GRIDSEARCHCV

# this is a great way of optimizing not only the model, but also all the previous
# processing steps at once.
# an important methos on pipeline is. Which allows us to print the list
# of parameters we can set for the whole list of estimators (transformers and classifiers)
# in our pipeline

## DEFINE SEARCH SPACE
space_std = {
    'rf':{'max_depth': hp.choice('max_depth', range(1,100)),
    'max_features': hp.uniform('max_features', 0, 1),
    'n_estimators': hp.choice('n_estimators', range(50,1000)),
    'criterion': hp.choice('criterion', ["gini", "entropy"]),
    'min_samples_split': scope.int(hp.uniform('min_samples_split', 2, 20)),
    'min_samples_leaf': scope.int(hp.uniform('min_samples_leaf', 1, 20))
    }
}

## DEFINE MODEL

def define_model(space_std):

    pipeline = Pipeline([
        ('rf', RandomForestClassifier(**space_std['rf'], random_state=42))
    ])

    return pipeline

## DEFINE TARGET FUNCTION

def fobj(space_std, X, y, ncv=5):

    model = define_model(space_std)
    cv_res = cross_val_score(model, X, y, cv=StratifiedKFold(ncv, random_state=42, shuffle=True), scoring='roc_auc', n_jobs=-1)

    return -cv_res.mean()

## DEFINE OPTIMIZATION

def optimize(X_train, y_train, max_evals=50):
    trials = Trials()
    best_params = fmin(partial(fobj, X=X_train, y=y_train),
                       space_std, algo=tpe.suggest, max_evals=max_evals,
                       trials=trials)
    return best_params, trials