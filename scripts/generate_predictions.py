"""
Script to generate predictions using the best model found on optimize_model script.
"""

import pickle
import pandas as pd
from src import preprocess
from src import model
from sklearn.model_selection import train_test_split
from hyperopt import space_eval
from sklearn.metrics import roc_auc_score, classification_report
import numpy as np

X, y = model.prepare_data()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, shuffle=True, stratify=y)
X_train, X_test = model.transform_data(X_train, X_test, X)

# Load best model
with open('data/models/best_params.pkl', 'rb') as file:
    best_params = pickle.load(file)

# If you get some error, please use the last best choice of parameters found, which is below.
# best_params = {'rf':{'criterion': 'entropy', 'max_depth': 28, 'max_features': 0.3722770714504038, 'min_samples_leaf': 11, 'min_samples_split': 12, 'n_estimators': 331}}

# Building and fitting classifier with best parameters
best_clf = model.define_model(best_params).fit(X_train, y_train)

# Calculating performance on validation set
y_test_pred = pd.DataFrame({'preds':best_clf.predict(X_test)})
test_idx = X_test.index
y_test_pred.index = test_idx
clf_val_score = roc_auc_score(y_test, best_clf.predict_proba(X_test)[:, 1])

# Print different metrics
print('Best parameters:')
print(best_params)
print('Validation AUC score: {:2.3f}'.format(clf_val_score))
print('Evaluation:')
print(classification_report(y_test, y_test_pred))

print('Most important features, in descending order of importance, are:\n')
sorted_index = np.argsort(-best_clf._final_estimator.feature_importances_)
cols_importance = X_train.columns[sorted_index].values
print(cols_importance[0:15])

# Append results to original observations and store predictions
# Loading data
df_job_desc = preprocess.load_data("job_desc.csv")
df_user = preprocess.load_data("user.csv")
df = df_user.merge(df_job_desc, how='outer', on='user_id')
predictions = df.merge(y_test_pred, how='inner', left_index=True, right_index=True)

# Export predictions to pickle object
with open('data/results/predictions.pkl', 'wb') as file:
     pickle.dump(predictions, file)

# Export predictions to csv
predictions.to_csv('data/results/predictions.csv')





