"""
Test script for applied_or_not project.
Let's try some ML models.
"""

#imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from src.preprocess import load_data

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, StandardScaler, MinMaxScaler
from sklearn.metrics import classification_report, roc_auc_score
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.metrics import make_scorer


#functions

def prepare_data():
    # Loading data
    df_job_desc = load_data("job_desc.csv")
    df_user = load_data("user.csv")

    df = df_user.merge(df_job_desc, how='outer', on='user_id')
    df.head()

    # create new feature telling if salary was nan or not
    df['salary_reported'] = ~df['salary'].isnull()

    # Generate onehot encodings
    enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
    enc.fit(df[['job_title_full', 'company']])
    # enc.fit(df[['company']])

    encoding = enc.transform(df[['job_title_full', 'company']])
    # encoding = enc.transform(df[['company']])

    encoding_names = enc.get_feature_names()
    df_encoded = pd.DataFrame(encoding, columns=encoding_names)
    new_df = pd.concat([df, df_encoded], sort=False, axis=1).drop(columns=['job_title_full', 'company'])

    # Separate into features, and target variable
    # new_df = df
    y = new_df.has_applied.astype('category')
    X = new_df[new_df.columns[~new_df.columns.isin(['user_id', 'has_applied'])]]
    return X,y

def transform_data(X_train, X_test):
    # Preprocess data, based on train so that there is no leakage
    train_idx = X_train.index
    test_idx = X_test.index
    imp_mean = SimpleImputer(missing_values=np.nan, strategy='mean')
    imp_mean.fit(X_train.select_dtypes(exclude='object'))
    X_train = pd.DataFrame(imp_mean.transform(X_train))
    X_test = pd.DataFrame(imp_mean.transform(X_test))
    X_train.columns = X.columns
    X_train.index = train_idx
    X_test.columns = X.columns
    X_test.index = test_idx

    scaler = MinMaxScaler()
    scaler.fit(np.array(X_train['salary']).reshape(-1, 1))
    X_train['salary'] = scaler.transform(np.array(X_train['salary']).reshape(-1, 1))
    X_test['salary'] = scaler.transform(np.array(X_test['salary']).reshape(-1, 1))

    return X_train, X_test

def define_model():

    pipeline = Pipeline([
        ('rf', RandomForestClassifier(random_state=42))
    ])

    # specify parameters for grid search
    parameters = {
        'rf__max_depth': [5,10,20,30,50,100],
        'rf__max_features': [0.5,0.75,0.8,0.95],
        'rf__max_leaf_nodes': [None, 10, 20, 30],
        'rf__max_samples': [None, 100, 1000],
        'rf__min_samples_leaf': [1, 5, 10, 100],
        'rf__min_samples_split': [0.5,0.75,1.0],
        'rf__n_estimators': [100,300,500,1000]
    }

    auc_scorer = make_scorer(roc_auc_score)
    #cv = GridSearchCV(pipeline, param_grid=parameters, verbose=10, scoring=auc_scorer)
    cv = RandomizedSearchCV(pipeline, param_grid=parameters, verbose=10, scoring=auc_scorer)

    return cv


#####################################################################################################################

# Split into train and test before anything else
# PIPELINES WIHT GRIDSEARCHCV

# this is a great way of optimizing not only the model, but also all the previous
# processing steps at once.
# an important methos on pipeline is. Which allows us to print the list
# of parameters we can set for the whole list of estimators (transformers and classifiers)
# in our pipeline

X, y = prepare_data()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)
X_train, X_test = transform_data(X_train, X_test)
cv = define_model()
cv.fit(X_train, y_train)
print("\nBest Parameters:", cv.best_params_)

import winsound
frequency = 1000  # Set Frequency To 2500 Hertz
duration = 1000  # Set Duration To 1000 ms == 1 second
winsound.Beep(frequency, duration)

# rf = RandomForestClassifier(random_state=42, max_depth=10, max_features=0.75, n_estimators=200,
#                             min_samples_leaf=5, min_samples_split=5)
# rf.fit(X_train, y_train)
#
# sorted_index = np.argsort(-rf.feature_importances_)
# rf.feature_importances_[sorted_index][0:60].sum()
# cols_importance = X_train.columns[sorted_index].values
# cols_importance[0:15]
#
# y_test_pred_rf = rf.predict(X_test)
# y_train_pred_rf = rf.predict(X_train)
# classification_report(y_train, y_train_pred_rf)
# classification_report(y_test, y_test_pred_rf)
# roc_auc_score(y_train, y_train_pred_rf)
# roc_auc_score(y_test, y_test_pred_rf)
#
# plt.plot(rf.feature_importances_[sorted_index].cumsum())










