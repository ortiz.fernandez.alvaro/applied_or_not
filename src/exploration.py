"""
This module contains functions for the exploratory data analysis section
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas_profiling import ProfileReport
import seaborn as sns
from src.preprocess import load_data, impute_mean
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

def generate_report(df, file, minimal=False):
    """Generate pandas profiling report and save it as html"""
    print('Generating Report...')
    profile = ProfileReport(df, minimal=minimal, title='Pandas Profiling Report', html={'style': {'full_width': True}})
    path = "data/reports/" + file
    profile.to_file(output_file=path)
    print('Report generated!')


def generate_figures(df):
    """Generate diverse visualization of V variables and save them in data/reports/figures"""
    print('Generating Figures...')
    cols_to_plot = df_user.columns[~df_user.columns.isin(['user_id', 'has_applied'])]
    applied = df_user.loc[df_user['has_applied']==1]
    not_applied = df_user.loc[df_user['has_applied']==0]

    for col in cols_to_plot:
        plt.figure()
        sns.distplot(df[col], label=col, hist=True, kde=True, rug=False)
        plt.savefig('data/reports/figures/' + col + '.png')
        plt.close()

    for col in cols_to_plot:
        plt.figure()
        sns.distplot(applied[col], label='applied', hist=False, kde=True, rug=False)
        sns.distplot(not_applied[col], label='not_applied', hist=False, kde=True, rug=False)
        plt.savefig('data/reports/figures/comparison_' + col + '.png')
        plt.close()

    print('Figures generated!')

def plot_pca(df):
    X = df.select_dtypes(exclude='object')
    X = StandardScaler().fit_transform(X)
    pca = PCA(n_components=None, svd_solver='full', random_state=42)
    pca.fit(X)
    plt.bar(x=range(0, pca.n_components_), height=pca.explained_variance_ratio_)
    plt.ylabel('Amount of variance explained (%)')
    plt.xlabel('Number of components')
    plt.savefig('data/reports/figures/pca_plot.png')
