"""Script to optimize model.

Using hyperopt package this script optimized the Random Forest Classifier iterating a max_evals number of times, and
storing the resulting best_params object with the best hyperparameters found under data/models.
"""

import pickle
from src import preprocess
from src import model
from sklearn.model_selection import train_test_split

X, y = model.prepare_data()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, shuffle=True, stratify=y)
X_train, X_test = model.transform_data(X_train, X_test, X)

## RUN OPTIMIZATION
best_params, trials = model.optimize(X_train, y_train, max_evals=50)

with open('data/models/best_params.pkl', 'wb') as file:
    pickle.dump(best_params, file, protocol=pickle.HIGHEST_PROTOCOL)

## LET US KNOW WHEN THE OPTIMIZATION IS COMPLETE BY MAKING BEEPING SOUND
import winsound
frequency = 1000  # Set Frequency To 2500 Hertz
duration = 3000  # Set Duration To 1000 ms == 1 second
winsound.Beep(frequency, duration)
