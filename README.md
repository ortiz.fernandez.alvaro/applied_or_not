# Applied Or Not

**Author:** Alvaro Ortiz (ortiz.fernandez.alvaro@gmail.com) <br>
**Date:** 12 March 2020 <br>

### Table of Contents

1. [Project Motivation](#project)

2. [Installation and File Descriptions](#file)

3. [Results](#results)

    - [Exploratory Data Analysis](#exploration) 
    - [Feature Engineering](#feature)
    - [Modelling](#modelling)
    - [Evaluation](#evaluation)
    - [Recommended next steps](#next)
    
4. [Licensing, Authors, and Acknowledgements](#licensing)

### Project Motivation <a name="project"></a> 

This is a purely fictitious example designed only for candidate assessment in recruitment.
The company MoreJobs4You publishes many different job openings online for users of their website to find and to apply for. 
Users of MoreJobs4You visit the MoreJobs4You website to browse through job openings of many different companies. 
The job openings were created and published by the respective company. If users find a job opening they like, 
they may choose to apply for the position directly via MoreJobs4You.

Since business is going well for MoreJobs4You and as they have collected some data on how users behave on their website, 
the team would now be interested in leveraging their data to further improve their offerings.
One of MoreJobs4You’s product owners has hired you as data science consultant to help the team with this task. 
To start with, the product owner would like to try whether one can predict whether a given user will not only read a job 
description, but also apply for it. To explore this question and to build a first prototype, the team provides you with 
a sample of their (actually much larger) data set.
For a subset of website users, the team provides you with two data sets:
- users.csv: Contains features describing the users. Due to privacy concerns, the names of these features have been 
anonymized.
- job_description.csv: Contains features describing the job description itself. It only contains those descriptions that 
have been visited by users contained in users.csv.
The goal of this prototype is to see whether one can predict the binary label has_applied (included in users.csv) which 
describes whether a user has applied to a given job posting. For that, both datasets may be used.

## Installation and File Descriptions <a name="file"></a>

The project is structured under different folders as follow:

- `data` contains all the figures and reports, although there is no raw data in there. Reason is that in the use case definition it is explicitely 
asked that no data is posted on the internet. Final predictions and model parameters are located under this folder though.
- `src` contains the different python custom modules that we will be using. They are split by functionality (preprocessing, model, etc)
- `scripts` this where the magic happens, and the go-to folder for any person insterested in the project. Here the scripts orchestrating the code are located,
under different names, each giving a hint at what they are trying to accomplish.
    - ``exploratory-analysis.py`` generates the reports under data/reports
    - ``optimize_model.py`` finds the best combination of hyperparameters for our model. Store them as a best_params object under data/models.
    - ``generate_predictions.py`` load the best hyperparameters stored under data/models, trains the model using those, and generates predictions
    on our test set. Predictions are stored under data/results.

## Results <a name="results"></a>

### Exploratory Data Analysis <a name="exploration"></a> 

The visualizations and reports created as a consequence of the analysis that ended up
with the generation of the results presented in this section can be found
in the [reports folder](/data/reports). They are mainly visualization in png format, as well as html reports created with 
[pandas-profiling](https://github.com/pandas-profiling/pandas-profiling).

I recommend you to take a look at the visualizations stored there, but if time is a constraint, 
you can just find the most important results listed below.

Regarding job_desc.csv: 
* There are 156 different `job_title_full`
* No missing values, except for `salary`, for which almost 70% of values are not filled.
Salaries are in the range 50-70k, with a mean and median of approx. 60k.
* 2000 different users in `user_id`, meaning that each row in the dataset is an interaction between one of the 156 jobs
and a certain user. **It is quite surprising that each user only has interacted with ONE job posting** but serves really
well for our purpose of predicting the value of `has_applied`, otherwise we would have duplicates and would need to rethink
how to deal with that.
* 8 companies in `company`, with roughly the same amount of posted jobs (in the 200 each in any case)


Regarding user.csv: 
* Target variable in here: `has_applied`. No imbalance; 57.6% users have applied and 42.4% have not.
* 56 numerical variables. All of them range from 0 to 1 in value. When it comes to missing values, only two of them, 
`v25` and `v30` have more than 50% of missing values. Most of the other have around 2-3%. Depending on the choice of
predicting algorithm we will see what methods do we use to deal with missing values.
* Since we have 56 features and 2000 observations, we would be good to go without needing to select a subset of features.
Remember the rule of thumb that we need 10 times more observations than features in the model in order to be able to train
it correctly. While this is just a rule of thumb, it is widely accepted as a starting point to do feature selection. We will
nonetheless look for correlations to see if we can get rid of some of them.
* You can find plots for every numerical variable under 
[reports folder](/data/reports), as well as the distribution of each variable for user who have applied and those who have not.
Just by looking at the kde density estimation by type of user, we can see that there is a difference between them. A good example
of plot where we can see such a difference in distributions is displayed below.

![v25](data/reports/figures/comparison_v25.png)

We would also be interested in calculating the correlation between features, and between features and the target
variable `has applied`, to see if there is any feature with a bigger linear predicting power than others, or even if we 
can reduce our feature space if we happen to find some of them to be highly correlated. Below the visualization of such
Pearson's correlations in the form of a heatmap. 

![corr](data/reports/figures/corr_heatmap.png)

Correlations among features are rather small. This might be a limitation for our task at hand here. Let's see what happens
with PCA, to see if we can further select some of them over others to train our model.

![pca](data/reports/figures/pca_plot.png)

**Conclusion**: We have not found a direct way to reduce the amount of features in our dataset without losing information.
There is no correlation between features, and there are no huge differences in the amount of variance explained by the
different component identified by PCA. Therefore, we will be training our ML models using all of them.

### Feature Engineering <a name="feature"></a> 

Some features have been engineered in order to improve the results of our model. Namely:
* basic NLP to extract info from job_title_full. Several columns have been created as boolean variables,
stating whether words such as 'senior', 'junior', 'remote', 'm/f/d', etc, were found.
* numerical encoding of company field
* Imputation of missing values using columns means.
* Tested whether taking out v25 and v30 (high percentage of missing values). Counterintuitively enough, Including them 
led to higher performance, so that has been our decision.
* Scaling of salary to 0-1 range with MinMaxScaler.

### Modelling <a name="modelling"></a> 

Several ML models tried and assesed. Random Forest Classifier the best performer. 
Hyperparameters tuning using hyperopt and random Grid search. Better hyperopt. Optimize to maximize AUC.

### Evaluation <a name="evaluation"></a> 

Evaluated on test set. Results are: 
* AUC: 0.725
* Precision on positive class: 0.69
* Recall on positive class: 0.80

### Conclusion and recommended next steps <a name="next"></a> 

While our model is not entirely prescriptive, it still tells us diverse means to improve the application ratio. 
For example, **reporting the salary leads to a much higher engagement with users**.

Other important features detected by the model are 'v30’,'v23’,'v45’,'v53’,'v25’ and salary in amount.

Additional improvements we can make with the data we already have include applying a more complex feature engineering with the
job title field. To begin with, text representations such as bag-of-words or tf-idf could be used.

Finally, having more diverse data on users could also benefit us. Not only job opening they have read, but also those which were showed to them as impressions, but not interacted with.

A simple and direct way to generate business value from this model would be
using it as a mean to predict the job positions any given user might apply to and generate recommendations 
upfront. This will likely lead to a increase in applications, which can be measured by running an experiment in the platform.

## Licensing, Authors, Acknowledgements <a name="licensing"></a>

This is an usecase designed for the interview for a Data Scientist position in the Corporate Decision Science unit at Bayer. 
Author is Alvaro Ortiz, who can be reached at ortiz.fernandez.alvaro@gmail.com.