"""
Test script for applied_or_not project.
Let's try some ML models.
"""

#imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from src.preprocess import load_data

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, StandardScaler
from sklearn.metrics import classification_report, roc_auc_score
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier


#functions

#####################################################################################################################

# Loading data
df_job_desc = load_data("job_desc.csv")
df_user = load_data("user.csv")

df = df_user.merge(df_job_desc, how='outer', on='user_id')
df.head()

#Generate onehot encodings
enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
enc.fit(df[['job_title_full','company']])
#enc.fit(df[['company']])

encoding = enc.transform(df[['job_title_full','company']])
#encoding = enc.transform(df[['company']])

encoding_names = enc.get_feature_names()
df_encoded = pd.DataFrame(encoding, columns=encoding_names)
new_df = pd.concat([df, df_encoded], sort=False, axis=1).drop(columns=['job_title_full','company'])

#Separate into features, and target variable
#new_df = df
y = new_df.has_applied.astype('category')
X = new_df[new_df.columns[~new_df.columns.isin(['user_id','has_applied'])]]
#X= X.drop(columns=['job_title_full','company','salary'])


# Split into train and test before anything else
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)
train_idx = X_train.index
test_idx = X_test.index

# Preprocess data, based on train so that there is no leakage
imp_mean = SimpleImputer(missing_values=np.nan, strategy='mean')
imp_mean.fit(X_train.select_dtypes(exclude='object'))
X_train = pd.DataFrame(imp_mean.transform(X_train))
X_test = pd.DataFrame(imp_mean.transform(X_test))
X_train.columns=X.columns
X_train.index=train_idx
X_test.columns=X.columns
X_test.index=test_idx

scaler = StandardScaler()
scaler.fit(np.array(X_train['salary']).reshape(-1, 1))
X_train['salary'] = scaler.transform(np.array(X_train['salary']).reshape(-1, 1))
X_test['salary'] = scaler.transform(np.array(X_test['salary']).reshape(-1, 1))

# model LogisticRegression
lreg = LogisticRegression(penalty='l2', C=1.0, fit_intercept=True, class_weight=None, random_state=42)
lreg.fit(X_train, y_train)

# model linearSVM
lsvm = LinearSVC(penalty='l2', C=1.0, fit_intercept=True, verbose=1, random_state=42)
lsvm.fit(X_train, y_train)

# model SVM
kernels = ['linear', 'poly', 'rbf', 'sigmoid', 'precomputed']
svc = SVC(kernel = kernels[3], C=1.0, decision_function_shape='ovo', verbose=True, gamma='auto')
svc.fit(X_train, y_train)

# model RF # TODO: Implement Pipeline with classifiers and crossvalidation
rf = RandomForestClassifier(random_state=42, max_depth=10, max_features=0.75, n_estimators=200,
                            min_samples_leaf=5, min_samples_split=5)
rf.fit(X_train, y_train)


sorted_index = np.argsort(-rf.feature_importances_)
rf.feature_importances_[sorted_index][0:60].sum()
cols_importance = X_train.columns[sorted_index].values

y_test_pred_rf = rf.predict(X_test)
y_train_pred_rf = rf.predict(X_train)
classification_report(y_train, y_train_pred_rf)
classification_report(y_test, y_test_pred_rf)
roc_auc_score(y_train, y_train_pred_rf)
roc_auc_score(y_test, y_test_pred_rf)

X_train_red = X_train[cols_importance[0:60]]
X_test_red = X_test[cols_importance[0:60]]
rf.fit(X_train_red, y_train)

y_test_pred_rf = rf.predict(X_test_red)
y_train_pred_rf = rf.predict(X_train_red)
classification_report(y_train, y_train_pred_rf)
classification_report(y_test, y_test_pred_rf)
roc_auc_score(y_train, y_train_pred_rf)
roc_auc_score(y_test, y_test_pred_rf)

# Prediction
y_test_pred_svc = svc.predict(X_test)
y_test_pred_lreg = lreg.predict(X_test)
y_test_pred_lsvm = lsvc.predict(X_test)
y_test_pred_rf = rf.predict(X_test)

y_train_pred_svc = svc.predict(X_train)
y_train_pred_lreg = lreg.predict(X_train)
y_train_pred_lsvm = lsvc.predict(X_train)
y_train_pred_rf = rf.predict(X_train)

# Evaluation
classification_report(y_test, y_test_pred_svc)
classification_report(y_test, y_test_pred_lreg)
classification_report(y_test, y_test_pred_lsvm)
classification_report(y_test, y_test_pred_rf)

classification_report(y_train, y_train_pred_svc)
classification_report(y_train, y_train_pred_lreg)
classification_report(y_train, y_train_pred_lsvm)
classification_report(y_train, y_train_pred_rf)

#train
roc_auc_score(y_train, y_train_pred_svc)
roc_auc_score(y_train, y_train_pred_lreg)
roc_auc_score(y_train, y_train_pred_lsvm)
roc_auc_score(y_train, y_train_pred_rf)

#test
roc_auc_score(y_test, y_test_pred_svc)
roc_auc_score(y_test, y_test_pred_lreg)
roc_auc_score(y_test, y_test_pred_lsvm)
roc_auc_score(y_test, y_test_pred_rf)



